# kotC9 Basic (mobile-basic)

A basic level app Ups2021
видос [тык](https://www.youtube.com/watch?v=p1Aihc6BQrA)

## Install the dependencies

```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)

```bash
quasar dev
```

### Lint the files

```bash
npm run lint
```

### Build the app for production

```bash
quasar build
```

### Customize the configuration

by kotC9 Sokolov Sergey

запускал с помощью

```bash
quasar dev -m android
```

See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).

use [this](https://quasar.dev/quasar-cli/developing-cordova-apps/preparation) guide to install on mobile device
and [build](https://quasar.dev/quasar-cli/developing-cordova-apps/build-commands#Developing) commands
